# Newlisp Modules

Various modules for the newlisp language.  The modules are primarily wrappers for C libraries.  Newlisp is welcome to incorporate them in the official distribution of newlisp.